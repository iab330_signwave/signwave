using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using MvvmCross.Core.ViewModels;
using SignWave.Core.ViewModels;
using Acr.UserDialogs;

namespace SignWave.Droid.Views
{
    [Activity(Label = "View for UserDeliveriesViewModel", MainLauncher = false)]
    [MvxViewFor(typeof(UserDeliveriesViewModel))]
    class UserDeliveriesView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.UserDeliveries);
            UserDialogs.Init(this);
        }
    }
}