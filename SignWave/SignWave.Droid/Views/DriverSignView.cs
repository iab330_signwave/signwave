using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using SignWave.Droid.Views;
using Android.Widget;
using Android.Views;
using MvvmCross.Core.ViewModels;
using SignWave.Core.ViewModels;
using System;
using System.Diagnostics;
using Acr.UserDialogs;
using Android.Nfc;
using Android.Text.Format;
using System.Text;
using Android.Content;

namespace SignWave.Droid.Views
{
    [Activity(Label = "DriverSignView")]
    public class DriverSignView : MvxActivity, View.IOnTouchListener, NfcAdapter.ICreateNdefMessageCallback, NfcAdapter.IOnNdefPushCompleteCallback
    {
        NfcAdapter nfcAdapter;
        TextView nfcText;
        private const int MESSAGE_SENT = 1;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.DriverSign);

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            ActionBar.Title = "SignWave (Driver)";

            nfcAdapter = NfcAdapter.GetDefaultAdapter(this);
            nfcText = FindViewById<TextView>(Resource.Id.nfcText);

            if (nfcAdapter == null) {
                nfcText.Text = "NFC is not available on this device";
            } else {
                nfcAdapter.SetNdefPushMessageCallback(this, this);
                nfcAdapter.SetOnNdefPushCompleteCallback(this, this);
            }

            UserDialogs.Init(this);
            FindViewById<ImageView>(Resource.Id.driverSignLogoButton).SetOnTouchListener(this);
        }

        public override bool OnCreateOptionsMenu(IMenu menu) {
            MenuInflater.Inflate(Resource.Menu.top_menu_driver, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            if (item.ItemId == Resource.Id.menu_settings) {
                StartActivity(typeof(DriverPreferencesView));
            } else if (item.ItemId == Resource.Id.menu_deliveries) {
                DriverSignViewModel model = (DriverSignViewModel)this.ViewModel;
                model.swapToDeliveries.Execute(null);
            }
            return base.OnOptionsItemSelected(item);
        }

        public bool OnTouch(View v, MotionEvent e) {
            if (v.Id == Resource.Id.driverSignLogoButton && e.Action == MotionEventActions.Down) {
                FindViewById<TextView>(Resource.Id.nfcText).Text = "kek";
            }
            return true;
        }






        public NdefMessage CreateNdefMessage(NfcEvent e) {
            var text = ("Signature Acknowledged");
            NdefMessage msg = new NdefMessage(new NdefRecord[] { CreateMimeRecord("application/com.android.signwave", Encoding.UTF8.GetBytes(text)) });
            return msg;
        }

        public void OnNdefPushComplete(NfcEvent e) {
            mHandler.ObtainMessage(MESSAGE_SENT).SendToTarget();
        }

        public NdefRecord CreateMimeRecord(String mimeType, byte[] payload) {
            byte[] mimeBytes = Encoding.UTF8.GetBytes(mimeType);
            NdefRecord mimeRecord = new NdefRecord(NdefRecord.TnfMimeMedia, mimeBytes, new byte[0], payload);
            return mimeRecord;
        }

        class NFCHandler : Handler
        {
            public NFCHandler(Action<Message> handler) {
                this.handle_message = handler;
            }
            Action<Message> handle_message;
            public override void HandleMessage(Message msg) {
                handle_message(msg);
            }
        }

        private readonly Handler mHandler;

        protected void HandlerHandleMessage(Message msg) {
            switch (msg.What) {
                case MESSAGE_SENT:
                    Toast.MakeText(this.ApplicationContext, "Message sent!", ToastLength.Long).Show();
                    break;
            }
        }

        protected override void OnResume() {
            base.OnResume();
            if (NfcAdapter.ActionNdefDiscovered == Intent.Action) {
                ProcessIntent(Intent);
            }
        }

        protected override void OnNewIntent(Intent intent) {
            Intent = intent;
        }

        void ProcessIntent(Intent intent) {
            IParcelable[] rawMsgs = intent.GetParcelableArrayExtra(NfcAdapter.ExtraNdefMessages);
            NdefMessage msg = (NdefMessage)rawMsgs[0];
            nfcText.Text = Encoding.UTF8.GetString(msg.GetRecords()[0].GetPayload());
        }
    }
}
