﻿using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using MvvmCross.Core.ViewModels;
using SignWave.Core.ViewModels;
using Acr.UserDialogs;
using Android.Widget;

namespace SignWave.Droid.Views
{
    [Activity(Label = "View for DriverDeliveriesViewModel", MainLauncher = false)]
    [MvxViewFor(typeof(DriverDeliveriesViewModel))]
    class DriverDeliveriesView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.DriverDeliveries);
            UserDialogs.Init(this);

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            ActionBar.Title = "Driver Deliveries";
        }
    }
}