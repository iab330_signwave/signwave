using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using SignWave.Droid.Views;
using Android.Widget;
using Android.Views;
using MvvmCross.Core.ViewModels;
using SignWave.Core.ViewModels;
using System;
using System.Diagnostics;
using Acr.UserDialogs;

namespace SignWave.Droid.Views
{
    [Activity(Label = "View for UserSignViewModel")]
    public class UserSignView : MvxActivity
    {
        Button button;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.UserSign);
            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            ActionBar.Title = "SignWave";
            UserDialogs.Init(this);
        }

        public override bool OnCreateOptionsMenu(IMenu menu) {
            MenuInflater.Inflate(Resource.Menu.top_menu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            if (item.ItemId == Resource.Id.menu_settings) {
                StartActivity(typeof(PreferencesView));
            } else if (item.ItemId == Resource.Id.menu_deliveries) {
                UserSignViewModel model = (UserSignViewModel)this.ViewModel;
                model.gotoDeliveries.Execute(null);
            } else if (item.ItemId == Resource.Id.menu_devices) {
                UserSignViewModel model = (UserSignViewModel)this.ViewModel;
                model.gotoDevices.Execute(null);
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}
