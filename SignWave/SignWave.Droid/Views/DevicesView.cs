using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using MvvmCross.Core.ViewModels;
using SignWave.Core.ViewModels;
using Android.Widget;

namespace SignWave.Droid.Views
{
    [Activity(Label = "View for DevicesViewModel", MainLauncher = false)]
    [MvxViewFor(typeof(DevicesViewModel))]
    class DevicesView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.devicesLayout);

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            ActionBar.Title = "Trusted Devices";
        }
    }
}