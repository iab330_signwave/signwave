using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using MvvmCross.Core.ViewModels;
using SignWave.Core.ViewModels;

namespace SignWave.Droid.Views
{
    [Activity(Label = "View for DriverDeliveriesViewModel")]
    class PortalView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Portal);
        }
    }
}