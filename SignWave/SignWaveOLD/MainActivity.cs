﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace SignWave
{
    [Activity(Label = "SignWave", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;


        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);


            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button Sign_button = FindViewById<Button>(Resource.Id.Signing);
            Button Deliver_button = FindViewById<Button>(Resource.Id.Deliveries);
            Button Settings_button = FindViewById<Button>(Resource.Id.Settings);

            Sign_button.Click += delegate {
                SetContentView(Resource.Layout.DriverSign); };
            Deliver_button.Click += delegate {
                SetContentView(Resource.Layout.Deliveries); };
            Settings_button.Click += delegate {
                SetContentView(Resource.Layout.Settings); };
            }
        public void nextPage()
        {

        }

        protected void OnDriverSignIn(Bundle bundle) {
            // Lol what does this even do?
            base.OnCreate(bundle);

            // Selecting the layout? Idk.
            //SetContentView(Resource.Layout.DriverSigning);

            // This is probably fine
            //ImageButton imgButton = FindViewById<ImageButton>(Resource.Id.MyButton);

            // Strange way to do a click event but ok whatever
            //imgButton.Click += delegate { search_for_nfc(imgButton); };
        }

        public void search_for_nfc(ImageButton btn){
            // This will change the image. To an animation?
        }
    }
}

