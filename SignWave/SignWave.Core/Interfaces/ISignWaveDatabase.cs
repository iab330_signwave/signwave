﻿using SignWave.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignWave.Core.Interfaces
{
    public interface ISignWaveDatabase
    {
        Task<IEnumerable<Delivery>> GetDeliveries();

        Task<int> InsertDelivery(Delivery delivery);

        Task<int> DeleteDilvery(object id);

        Task<int> AlterDeliveryStatus(object id);

        Task<bool> CheckIfExists(Delivery delivery);

        Task ClearDeliveries();

        Task<IEnumerable<Device>> GetDevices();

        Task<int> DeleteDevice(object id);

        Task<int> InsertDevice(Device device);

        Task<bool> CheckIfExists(Device device);

        Task clearDevices();
    }
}
