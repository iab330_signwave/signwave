﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SQLite.Net;
using SignWave.Core.Interfaces;
using SignWave.Core.Models;
using MvvmCross.Platform;
using System.Threading.Tasks;

namespace SignWave.Core.Database
{
    public class SignWaveDatabase : ISignWaveDatabase
    {
        private SQLiteConnection database;
        public SignWaveDatabase() {
            var sqlite = Mvx.Resolve<ISqlite>();
            database = sqlite.GetConnection();
            database.CreateTable<Device>();
            database.CreateTable<Delivery>();
        }

        public async Task<IEnumerable<Delivery>> GetDeliveries() {
            return database.Table<Delivery>().ToList();
        }

        public async Task<int> InsertDelivery(Delivery delivery) {
            var num = database.Insert(delivery);
            database.Commit();
            return num;
        }

        public async Task<int> DeleteDilvery(object id) {
            return database.Delete<Delivery>(Convert.ToInt16(id));
        }

        public async Task<int> AlterDeliveryStatus(object id) {
            database.Update(id);
            //SQLiteCommand command = new SQLiteCommand();
            //conn.query<type>("query)
            //database.Update<Delivery>(id, typeof(Status));
            return 1;
        }

        public async Task<bool> CheckIfExists(Delivery delivery) {
            var exists = database.Table<Delivery>()
            .Any(x => x.id == delivery.id);
            return exists;
        }

        public async Task ClearDeliveries() {
            database.DeleteAll<Delivery>();
        }

        public async Task<IEnumerable<Device>> GetDevices() {
            return database.Table<Device>().ToList();
        }

        public async Task<int> DeleteDevice(object id) {
            return database.Delete<Device>(Convert.ToInt16(id));
        }

        public async Task<int> InsertDevice(Device device) {
            if (device.ID == null) return -1;
            var num = database.Insert(device);
            database.Commit();
            return num;
        }

        public async Task<bool> CheckIfExists(Device device) {
            var exists = database.Table<Device>()
            .Any(x => x.ID == device.ID);
            return exists;
        }

        public async Task clearDevices() {
            database.DeleteAll<Device>();
        }
    }
}
