﻿using Acr.UserDialogs;
using MvvmCross.Core.ViewModels;
using SignWave.Core.Database;
using SignWave.Core.Interfaces;
using SignWave.Core.Models;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SignWave.Core.ViewModels
{
    public class DriverDeliveriesViewModel
       : MvxViewModel
    {
        public int currentDeliveryID;
        public ICommand selectDeliveryCommand { get; private set; }
        private readonly ISignWaveDatabase database;

        public DriverDeliveriesViewModel(ISignWaveDatabase db)
        {
            database = db;
            DeliveryIDs = new ObservableCollection<Delivery>();

            readDBFunc();
       
            selectDeliveryCommand = new MvxCommand<Delivery>(delivery => {
                DeliveryID = delivery.id;
                Address = delivery.address;
                XStatus = delivery.status;
            });
        }

        public async void readDBFunc() {
            //await database.InsertDelivery(new Delivery("743 Evergreen Terrace", "Item", Status.inProgress, DateTime.Now));
            //await database.InsertDelivery(new Delivery("742 Evergreen Terrace", "Item2", Status.inProgress, DateTime.Now));
            //await database.InsertDelivery(new Delivery("741 Evergreen Terrace", "Item3", Status.inProgress, DateTime.Now));
            var deliveries = await database.GetDeliveries();
            foreach (var d in deliveries) {
                DeliveryIDs.Add(d);
            }
            RaisePropertyChanged(() => DeliveryIDs);
        }

        public void AddDelivery(Delivery delivery)
        {
            if (delivery.id != 0)
            {
                DeliveryIDs.Add(delivery);
            }
        }

        private ObservableCollection<Delivery> deliveryIDs;
        public ObservableCollection<Delivery> DeliveryIDs
        {
            get
            {
                return deliveryIDs;
            }
            set
            {
                SetProperty(ref deliveryIDs, value);
            }
        }

        private int deliveryID;
        public int DeliveryID
        {
            get
            {
                return deliveryID;
            }
            set
            {
                SetProperty(ref deliveryID, value);
            }
        }
        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (value != null)
                {
                    SetProperty(ref address, value);
                }
            }
        }
        private Status status;
        public Status XStatus
        {
            get
            {
                return status;
            }
            set
            {
                SetProperty(ref status, value);
            }
        }
        public ActionSheetConfig declareActionSheetConfig(String item)
        {
            ActionSheetConfig ASC = new ActionSheetConfig();
            ASC.Title = item;
            ASC.Add("Delivery Settings", new System.Action(DeliverySettings));
            ASC.Add("Sign for Delivery", new System.Action(SigningScreen));
            return ASC;
        }

        private void DeliverySettings()
        {
            // Bring up delivery settings
        }

        private void SigningScreen()
        {
            // Swap to signing screen
            ShowViewModel<DriverSignViewModel>(new DeliveryIdentifier() { DeliveryID = currentDeliveryID });
        }

        public class DeliveryIdentifier
        {
            public int DeliveryID { get; set; }
        }
    }
}