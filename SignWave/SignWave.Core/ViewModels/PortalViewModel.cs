﻿using MvvmCross.Core.ViewModels;
using SignWave.Core.Database;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SignWave.Core.ViewModels
{
    public class PortalViewModel
        : MvxViewModel
    {
        public ICommand userCommand { get; private set; }
        public ICommand driverCommand { get; private set; }
        public PortalViewModel() {
            userCommand = new MvxCommand(() => {
                ShowViewModel<UserSignViewModel>();
                });
            driverCommand = new MvxCommand(() => {
                ShowViewModel<DriverSignViewModel>();
            });
        }

    }
}
