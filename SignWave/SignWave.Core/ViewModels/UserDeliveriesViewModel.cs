﻿using Acr.UserDialogs;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using SignWave.Core.Database;
using SignWave.Core.Models;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SignWave.Core.ViewModels
{
    public class UserDeliveriesViewModel
       : MvxViewModel
    {
        public int currentDeliveryID;
        public ICommand swapToSettings { get; private set; }
        public ICommand swapToSigning { get; private set; }
        public ICommand selectDeliveryCommand { get; private set; }
        public ICommand failToSign { get; private set; }
        public UserDeliveriesViewModel()
        {
            DeliveryIDs = new ObservableCollection<Delivery>();

            // read all deliveries, one by one add with AddDelivery
            // then call raisepropertychanged at the end

            //buttoncommand = new mvxcommand(() =>
            //{
            //    adddelivery(new delivery(deliveryID, address));
            //    raisepropertychanged(() => deliveryIDs);
            //});

            selectDeliveryCommand = new MvxCommand<Delivery>(delivery => {
                ActionSheetConfig ASC = declareActionSheetConfig(delivery.item);
                failToSign = new MvxCommand(() => Mvx.Resolve<IUserDialogs>().ActionSheet(ASC));
            });
            //swapToSettings = new MvxCommand(() => ShowViewModel<SettingsViewModel>());
            swapToSigning = new MvxCommand(() => ShowViewModel<DriverSignViewModel>());
        }
        public void AddDelivery(Delivery delivery)
        {
            if (delivery.id != 0)
            {
                DeliveryIDs.Add(delivery);
            }
        }

        private ObservableCollection<Delivery> deliveryIDs;
        public ObservableCollection<Delivery> DeliveryIDs
        {
            get
            {
                return deliveryIDs;
            }
            set
            {
                SetProperty(ref deliveryIDs, value);
            }
        }

        private string deliveryID;
        public string DeliveryID
        {
            get
            {
                return deliveryID;
            }
            set
            {
                if (value != null)
                {
                    SetProperty(ref deliveryID, value);
                }
            }
        }
        private string item;
        public string Item
        {
            get
            {
                return item;
            }
            set
            {
                if (value != null)
                {
                    SetProperty(ref item, value);
                }
            }
        }
        private System.DateTime date;
        public System.DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                if (value != null)
                {
                    SetProperty(ref date, value);
                }
            }
        }

        public ActionSheetConfig declareActionSheetConfig(String item)
        {
            ActionSheetConfig ASC = new ActionSheetConfig();
            ASC.Title = item;
            ASC.Add("Delivery Settings", new System.Action(DeliverySettings));
            ASC.Add("Sign for Delivery", new System.Action(SigningScreen));
            return ASC;
        }

        private void DeliverySettings()
        {
            // Bring up delivery settings
        }

        private void SigningScreen()
        {
            // Swap to signing screen
            ShowViewModel<DriverSignViewModel>(new DeliveryIdentifier() { DeliveryID = currentDeliveryID });
        }

        public class DeliveryIdentifier
        {
            public int DeliveryID { get; set; }
        }
    }
}