﻿using MvvmCross.Core.ViewModels;
using SignWave.Core.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SignWave.Core.Interfaces;
using SignWave.Core.Database;
using MvvmCross.Platform;

namespace SignWave.Core.ViewModels
{
    public class DevicesViewModel : MvxViewModel
    {
        private readonly ISignWaveDatabase devicesDatabase;

        private string deviceID;
        public string DeviceID
        {
            get { return deviceID; }
            set { SetProperty(ref deviceID, value); }
        }

        private string devices;
        public string Devices
        {
            get { return devices; }
            set { SetProperty(ref devices, value); }
        }

        public ICommand addToDBCommand { get; private set; }
        public ICommand readDBCommand { get; private set; }
        public ICommand clearDBCommand { get; private set; }
        public ICommand backButtonCommand { get; private set; }


        public DevicesViewModel(ISignWaveDatabase devicesDatabase) {
            this.devicesDatabase = devicesDatabase;
            addToDBCommand = new MvxCommand(() => addToDBFunc(new Device { ID = DeviceID }));
            readDBCommand = new MvxCommand(() => readDBFunc());
            clearDBCommand = new MvxCommand(() => clearDBFunc());
            backButtonCommand = new MvxCommand(() => backButtonFunc());
            readDBFunc();
        }

        private void backButtonFunc() {
            Close(this);
        }

        public async void readDBFunc() {
            Devices = "";
            var deviceList = await devicesDatabase.GetDevices();
            foreach(var d in deviceList) {
                Devices += d.ID + ", ";
            }
        }

        public async void addToDBFunc(Device xdevice) {
            if (!await devicesDatabase.CheckIfExists(xdevice)) {
                await devicesDatabase.InsertDevice(xdevice);

                //Close(this);
                readDBFunc();
            }
        }

        public async void clearDBFunc() {
            await devicesDatabase.clearDevices();
            readDBFunc();
        }
    }
}
