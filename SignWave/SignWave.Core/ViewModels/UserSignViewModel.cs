﻿using MvvmCross.Core.ViewModels;
using SignWave.Core.Database;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using SignWave.Core.ViewModels;

namespace SignWave.Core.ViewModels
{
    public class UserSignViewModel
        : MvxViewModel
    {
        public ICommand gotoDeliveries { get; private set; }
        public ICommand gotoDevices { get; private set; }
        public UserSignViewModel() {
            gotoDeliveries = new MvxCommand(() => ShowViewModel<UserDeliveriesViewModel>());
            gotoDevices = new MvxCommand(() => ShowViewModel<DevicesViewModel>());
        }

    }
}
