﻿using MvvmCross.Core.ViewModels;
using SignWave.Core.Database;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using SignWave.Core.ViewModels;
using Acr.UserDialogs;
using MvvmCross.Platform;

namespace SignWave.Core.ViewModels
{
    public class DriverSignViewModel
        : MvxViewModel
    {
        //public ICommand swapToSettings { get; private set; }
        public ICommand swapToDeliveries { get; private set; }
        public ICommand failToSign { get; private set; }
        public DriverSignViewModel()
        {
            ActionSheetConfig ASC = declareActionSheetConfig();
            //swapToSettings = new MvxCommand(() => ShowViewModel<SettingsViewModel>());
            swapToDeliveries = new MvxCommand(() => ShowViewModel<DriverDeliveriesViewModel>());
            failToSign = new MvxCommand(() => Mvx.Resolve<IUserDialogs>().ActionSheet(ASC));
        }

    public ActionSheetConfig declareActionSheetConfig()
    {
        ActionSheetConfig ASC = new ActionSheetConfig();
        ASC.Title = "NFC Signing failed";
        ASC.Add("Try again");
        ASC.Add("Change Package");
        ASC.Add("Manual Sign");
        return ASC;
    }
}
}
