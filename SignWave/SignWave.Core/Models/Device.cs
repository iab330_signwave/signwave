﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;

namespace SignWave.Core.Models
{
    public class Device
    {
        [PrimaryKey]
        public string ID { get; set; }
    }
}
