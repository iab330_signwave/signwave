﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;

namespace SignWave.Core.Models
{
    public enum Status { inProgress, delivered, failed };
    public class Delivery {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string address { get; set; }
        public string item { get; set; }
        public Status status { get; set; }
        public DateTime deliveryDate { get; set; }

        public Delivery() {
            this.address = "";
            this.item = "";
            this.status = Status.failed;
            this.deliveryDate = DateTime.Now;
        }

        public Delivery(string address, string item, Status status, DateTime deliveryDate) {
            this.address = address;
            this.item = item;
            this.status = status;
            this.deliveryDate = deliveryDate;
        }
    }
}
